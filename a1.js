db.fruits.aggregate([{ $match: { onSale: true } }, { $count: "onSale" }]);

db.fruits.aggregate([
  { $match: { stock: { $gt: 20 } } },
  { $count: "Fruits gt 20 stocks" },
]);

db.fruits.aggregate([
  { $match: { onSale: true } },
  { $group: { _id: "$supplier_id", avgPrice: { $avg: "$price" } } },
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", maxPrice: { $max: "$price" } } },
]);

db.fruits.aggregate([
  { $group: { _id: "$supplier_id", minPrice: { $min: "$price" } } },
]);
